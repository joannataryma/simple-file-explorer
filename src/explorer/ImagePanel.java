package explorer;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

public class ImagePanel extends JComponent {

    public ImagePanel(int szerokosc, int wysokosc) {
        setSize(szerokosc, wysokosc);
    }

    public void ustawObraz(String sciezka) {
        try {
            image = ImageIO.read(new File(sciezka));
        } catch (IOException ex) {
            image = null;
        } finally {
            repaint();
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        int w = 0, h = 0, x = 0, y = 0;

        if (image != null) {
            double ratio1 = (double) image.getWidth(null) / getWidth();
            double ratio2 = (double) image.getHeight(null) / getHeight();

            double ratio;
            if (ratio1 > ratio2) {
                ratio = ratio1;
            } else {
                ratio = ratio2;
            }

            w = (int) (image.getWidth(null) / ratio);
            h = (int) (image.getHeight(null) / ratio);

            x = (getWidth() - w) / 2;
            y = (getHeight() - h) / 2;
        }

        g.drawImage(image, x, y, w, h, null);
    }
    private Image image = null;
}

package explorer;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class File2 extends File {

    File2(File p_file) {
        super(p_file.getPath());
    }

    public String fileInfo() {
        SimpleDateFormat wdata = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        String data = wdata.format(new Date(lastModified()));
        String name = getName();
        String size = Long.toString(length()) + " B";

        return ("<html><font size=+1>" + name + "</font><br><br>size: " + size + "<br>last modified: " + data);
    }

    @Override
    public String toString() {
        return getName();
    }
}


package explorer;

import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

public class ExplorerWindow extends JFrame implements TreeSelectionListener
{
    public ExplorerWindow() 
    {
        initComponents();

        wyborPliku.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 

        fileTreeExplorer.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        fileTreeExplorer.addTreeSelectionListener(this);

        wypelnij_drzewo("D:\\"); 

        imagePanel=new ImagePanel(picturePanel.getWidth(),picturePanel.getHeight());
        picturePanel.add(imagePanel); 
        fileDialog = new NewFileForm(this); 
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) 
    {
        File2 plik = null;
        Object wezel = fileTreeExplorer.getLastSelectedPathComponent();

        if(wezel instanceof File2)
        {
            plik = (File2)fileTreeExplorer.getLastSelectedPathComponent();
        }
        else
        {
            fileInfoLabel.setText("");
            return;
        }

        if(plik == null) return;

        imagePanel.ustawObraz(plik.getPath());
        if(plik.isDirectory()) fileInfoLabel.setText("");
        else fileInfoLabel.setText(plik.fileInfo());
    }

    public void dodaniePliku()
    {
        fileDialog.setVisible(false);

        File plik = (File)fileTreeExplorer.getLastSelectedPathComponent();
        File newFile = new File(plik.getPath() + "\\" + fileDialog.getFileName());

        if(fileDialog.isDirectory())
        {
            if(!newFile.mkdir())
            {
                JOptionPane.showMessageDialog(this,"Can't create a folder.",
                        "Error",JOptionPane.WARNING_MESSAGE);
                return;
            }
        }
        else
        {
            try
            {
                if(!newFile.createNewFile())
                {
                    JOptionPane.showMessageDialog(this,"Can't create a file.",
                            "Error",JOptionPane.WARNING_MESSAGE);
                    return;
                }
            }
            catch(IOException ex)
            {
                JOptionPane.showMessageDialog(this,"Can't create a file.",
                        "Error",JOptionPane.WARNING_MESSAGE);
                return;
            }
        }

        TreePath wezel=fileTreeExplorer.getSelectionPath();
        String nazwa=fileTreeExplorer.getModel().getRoot().toString();
        wypelnij_drzewo(nazwa);
        fileTreeExplorer.expandPath(wezel);
    }

        private void wypelnij_drzewo(String sciezka)
    {
        File plik=new File(sciezka);

        TreeModel model=new FileTreeModel(plik);

        fileTreeExplorer.setModel(model);
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ExplorerWindow().setVisible(true);
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileInfoFramePanel = new javax.swing.JPanel();
        fileInfoLabel = new javax.swing.JLabel();
        fileTreeScrollPane = new javax.swing.JScrollPane();
        fileTreeExplorer = new javax.swing.JTree();
        pictureFramePanel = new javax.swing.JPanel();
        picturePanel = new javax.swing.JPanel();
        menuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("File Browser");
        setBounds(new java.awt.Rectangle(100, 100, 0, 0));
        setResizable(false);

        fileInfoFramePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("File info"));
        fileInfoFramePanel.setName("fileInfoFramePanel"); // NOI18N

        fileInfoLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        fileInfoLabel.setName("fileInfoLabel"); // NOI18N
        fileInfoLabel.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout fileInfoFramePanelLayout = new javax.swing.GroupLayout(fileInfoFramePanel);
        fileInfoFramePanel.setLayout(fileInfoFramePanelLayout);
        fileInfoFramePanelLayout.setHorizontalGroup(
            fileInfoFramePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fileInfoFramePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fileInfoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
        );
        fileInfoFramePanelLayout.setVerticalGroup(
            fileInfoFramePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fileInfoLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        fileTreeScrollPane.setName("fileTreeScrollPane"); // NOI18N

        fileTreeExplorer.setName("fileTreeExplorer"); // NOI18N
        fileTreeExplorer.setShowsRootHandles(true);
        fileTreeScrollPane.setViewportView(fileTreeExplorer);

        pictureFramePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Picture"));
        pictureFramePanel.setName("pictureFramePanel"); // NOI18N

        picturePanel.setName("picturePanel"); // NOI18N

        javax.swing.GroupLayout picturePanelLayout = new javax.swing.GroupLayout(picturePanel);
        picturePanel.setLayout(picturePanelLayout);
        picturePanelLayout.setHorizontalGroup(
            picturePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 390, Short.MAX_VALUE)
        );
        picturePanelLayout.setVerticalGroup(
            picturePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 296, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pictureFramePanelLayout = new javax.swing.GroupLayout(pictureFramePanel);
        pictureFramePanel.setLayout(pictureFramePanelLayout);
        pictureFramePanelLayout.setHorizontalGroup(
            pictureFramePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pictureFramePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(picturePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pictureFramePanelLayout.setVerticalGroup(
            pictureFramePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pictureFramePanelLayout.createSequentialGroup()
                .addComponent(picturePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        menuBar.setName("menuBar"); // NOI18N

        jMenu1.setText("File");
        jMenu1.setName("jMenu1"); // NOI18N

        jSeparator1.setName("jSeparator1"); // NOI18N
        jMenu1.add(jSeparator1);

        jMenuItem4.setText("New Folder\\File");
        jMenuItem4.setName("jMenuItem4"); // NOI18N
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuItem1.setText("Open Folder...");
        jMenuItem1.setName("jMenuItem1"); // NOI18N
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);
        jMenu1.add(jSeparator2);

        jMenuItem2.setText("Close");
        jMenuItem2.setName("jMenuItem2"); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        menuBar.add(jMenu1);

        jMenu2.setText("About");
        jMenu2.setName("jMenu2"); // NOI18N

        jMenuItem3.setText("About...");
        jMenuItem3.setName("jMenuItem3"); // NOI18N
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(fileTreeScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fileInfoFramePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pictureFramePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pictureFramePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fileInfoFramePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(fileTreeScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        if(wyborPliku.showOpenDialog(this)==JFileChooser.APPROVE_OPTION)
        {
            wypelnij_drzewo(wyborPliku.getSelectedFile().getPath());
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        JOptionPane.showMessageDialog(this,"Author: Mariusz Redwanz gr 6B","About",JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {   
        File plik = (File)fileTreeExplorer.getLastSelectedPathComponent();
        if(plik == null || plik.isFile())
        {
            JOptionPane.showMessageDialog(this, "Choose a folder.");
            return;
        }
        fileDialog.setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel fileInfoFramePanel;
    private javax.swing.JLabel fileInfoLabel;
    private javax.swing.JTree fileTreeExplorer;
    private javax.swing.JScrollPane fileTreeScrollPane;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JPanel pictureFramePanel;
    private javax.swing.JPanel picturePanel;
    // End of variables declaration//GEN-END:variables
    private JFileChooser wyborPliku = new JFileChooser();
    private ImagePanel imagePanel;
    private NewFileForm fileDialog;
}

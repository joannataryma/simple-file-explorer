package explorer;

import java.io.File;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class FileTreeModel implements TreeModel {

    private File m_root;

    public FileTreeModel(File p_root) {
        this.m_root = p_root;
    }

    @Override
    public Object getRoot() {
        return m_root;
    }

    @Override
    public Object getChild(Object parent, int index) {
        File[] children = ((File) parent).listFiles();

        if (children == null || index < 0 || index >= children.length) {
            return null;
        }

        File wynik = new File2(children[index]);

        return wynik;
    }

    @Override
    public int getChildCount(Object parent) {
        String[] children = ((File) parent).list();

        if (children == null) {
            return 0;
        }

        return children.length;
    }

    @Override
    public boolean isLeaf(Object node) {
        if (((File) node).isFile()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        File p = (File) parent;
        File c = (File) child;

        if (parent == null || child == null) {
            return -1;
        }

        File[] children = p.listFiles();

        for (int i = 0; i < p.length(); ++i) {
            if (children[i].equals(c)) {
                return i;
            }
        }

        return -1;
    }

    /*
     * Below functions are not used yet.
     * 
     */
    @Override
    public void addTreeModelListener(TreeModelListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

}

package explorer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class NewFileForm extends JFrame {

    public NewFileForm(final ExplorerWindow mainWindow) {
        setTitle("New Folder\\File");
        setResizable(false);
        setSize(250, 150);

        setLocation(
                mainWindow.getX() + mainWindow.getWidth() / 2 - getWidth() / 2,
                mainWindow.getY() + mainWindow.getHeight() / 2 - getHeight() / 2);

        nameTextField = new JTextField();
        nameTextField.setText("");
        isDirectoryCheckBox = new JCheckBox();
        isDirectoryCheckBox.setText("New Folder");
        isDirectoryCheckBox.setSelected(false);
        JPanel northPanel = new JPanel();
        northPanel.setLayout(new GridLayout(3, 1));
        northPanel.add(new JLabel("File name: "));
        northPanel.add(nameTextField);
        northPanel.add(isDirectoryCheckBox);

        add(northPanel, BorderLayout.NORTH);

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new GridLayout(1, 2));

        cancelButton = new JButton("Cancel");
        cancelButton.setSelected(true);
        southPanel.add(cancelButton);

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                setVisible(false);
                nameTextField.setText("");
                isDirectoryCheckBox.setSelected(false);
            }
        });

        okButton = new JButton("OK");
        okButton.setSelected(true);
        southPanel.add(okButton);

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                setVisible(false);
                mainWindow.dodaniePliku();
                nameTextField.setText("");
                isDirectoryCheckBox.setSelected(false);
            }
        });

        add(southPanel, BorderLayout.SOUTH);
    }

    public String getFileName() {
        return nameTextField.getText();
    }

    public boolean isDirectory() {
        return isDirectoryCheckBox.isSelected();
    }
    private JButton cancelButton;
    private JButton okButton;
    private JTextField nameTextField;
    private JCheckBox isDirectoryCheckBox;
}
